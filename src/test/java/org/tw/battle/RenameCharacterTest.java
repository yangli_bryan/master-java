package org.tw.battle;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.tw.battle.dbTest.InMemoryDbSupport;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.Game;

import static org.junit.jupiter.api.Assertions.*;
import static org.tw.battle.GameTestHelper.createCharacter;
import static org.tw.battle.GameTestHelper.createGame;
import static org.tw.battle.dbTest.TestQueryHelper.getCharacterName;

@InMemoryDbSupport
class RenameCharacterTest {
    @Test
    void should_rename_character() throws Exception {
        final Game game = createGame();
        final Integer id = createCharacter(game);

        final String expectedName = "my-awesome-name";
        final CommandResponse response = game.execute("rename-character", id.toString(), expectedName);

        assertTrue(response.isSuccess());
        assertEquals(expectedName, getCharacterName(id));
    }

    @Test
    void should_rename_character_with_longest_possible_name_length() throws Exception {
        final Game game = createGame();
        final Integer id = createCharacter(game);

        final String expectedName = createString('a', 64);
        final CommandResponse response = game.execute("rename-character", id.toString(), expectedName);

        assertTrue(response.isSuccess());
        assertEquals(expectedName, getCharacterName(id));
    }

    @Test
    void should_fail_if_no_argument_is_provided() {
        final Game game = createGame();
        final CommandResponse response = game.execute("rename-character");

        assertFalse(response.isSuccess());
        assertEquals("Bad command: rename-character <character id> <new name>", response.getMessage());
    }

    @Test
    void should_not_change_name_if_no_argument_is_provided() throws Exception {
        final Game game = createGame();
        final Integer id = createCharacter(game);
        game.execute("rename-character");

        assertEquals("unnamed", getCharacterName(id));
    }

    @Test
    void should_fail_if_no_new_name_is_provided() {
        final Game game = createGame();
        final Integer id = createCharacter(game);
        final CommandResponse response = game.execute("rename-character", id.toString());

        assertFalse(response.isSuccess());
        assertEquals("Bad command: rename-character <character id> <new name>", response.getMessage());
    }

    @Test
    void should_not_change_name_if_no_new_name_is_provided() throws Exception {
        final Game game = createGame();
        final Integer id = createCharacter(game);
        game.execute("rename-character", id.toString());

        assertEquals("unnamed", getCharacterName(id));
    }

    @ParameterizedTest
    @ValueSource(strings = {"contains space", "{}", "***", "a_b_c", "ab"})
    void should_fail_if_name_violates_rules(String invalidName) throws Exception {
        final Game game = createGame();
        final Integer id = createCharacter(game);
        final CommandResponse response = game.execute("rename-character", id.toString(), invalidName);

        assertFalse(response.isSuccess());
        assertEquals("Bad command: rename-character <character id> <new name>", response.getMessage());
        assertEquals("unnamed", getCharacterName(id));
    }

    @Test
    void should_fail_if_name_is_greater_than_64_characters() throws Exception {
        final Game game = createGame();
        final Integer id = createCharacter(game);
        final CommandResponse response = game.execute("rename-character", id.toString(), createString('c', 65));

        assertFalse(response.isSuccess());
        assertEquals("Bad command: rename-character <character id> <new name>", response.getMessage());
        assertEquals("unnamed", getCharacterName(id));
    }

    @Test
    void should_fail_if_id_is_not_a_number() throws Exception {
        final Game game = createGame();
        final Integer id = createCharacter(game);
        final CommandResponse response = game.execute("rename-character", "0 OR 0=0", "new-name");

        assertFalse(response.isSuccess());
        assertEquals("Bad command: rename-character <character id> <new name>", response.getMessage());
        assertEquals("unnamed", getCharacterName(id));
    }

    @Test
    void should_fail_if_character_does_not_exist() {
        final Game game = createGame();
        final String notExistId = "9999";
        final CommandResponse response = game.execute("rename-character", notExistId, "new-name");

        assertFalse(response.isSuccess());
        assertEquals("Bad command: character not exist", response.getMessage());
    }

    @Test
    void should_get_character_information_after_name_is_changed() {
        final Game game = createGame();
        final Integer id = createCharacter(game);
        game.execute("rename-character", id.toString(), "new-name");
        final CommandResponse response = game.execute("character-info", id.toString());

        assertTrue(response.isSuccess());
        assertEquals(
            String.format("id: %d, name: new-name, hp: 100, x: 0, y: 0, status: alive", id),
            response.getMessage());
    }

    private String createString(char c, int repeat) {
        final StringBuilder builder = new StringBuilder(repeat + 1);
        for (int i = 0; i < repeat; i++) {
            builder.append(c);
        }

        return builder.toString();
    }
}
