package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Liu Xia
 */
public class CharacterRepository {
    private final ServiceConfiguration configuration;

    public CharacterRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public int create(String name, int hp, int x, int y) throws Exception {
        Connection conn = DatabaseConnectionProvider.createConnection(configuration);
        Statement statement = conn.createStatement();

        String insertQuery = "INSERT INTO `character` (`name`, hp, x, y) VALUES (?, ?, ?, ?)";
        PreparedStatement preparedStatement = conn.prepareStatement(insertQuery);

        preparedStatement.setString(1, name);
        preparedStatement.setInt(2, hp);
        preparedStatement.setInt(3, x);
        preparedStatement.setInt(4, y);

        preparedStatement.execute();

        ResultSet rs = statement.executeQuery("SELECT id FROM `character` WHERE name = \'" + name + "\'");
        rs.next();
        int returnedId = rs.getInt("id");

        conn.close();
        return returnedId;
    }

    public String getInfo(int id) throws Exception {
        Connection conn = DatabaseConnectionProvider.createConnection(configuration);
        Statement statement = conn.createStatement();

        String getInfoQuery = "SELECT * FROM `character` WHERE id = " + id ;
        ResultSet rs = statement.executeQuery(getInfoQuery);

        String characterInfo;
        if (rs.next()) {
            String characterName = rs.getString("name");
            int characterHp = rs.getInt("hp");
            int characterX = rs.getInt("x");
            int characterY = rs.getInt("y");
            String characterStatus = characterHp > 0 ? "alive" : "dead";
            characterInfo = String.format("id: " + id + ", name: " + characterName + ", hp: " + characterHp +
                   ", x: " + characterX + ", y: " + characterY + ", status: " + characterStatus);
        } else {
            characterInfo = "character not exist";
        }
        conn.close();
        return characterInfo;
    }

    public String updateName(int id, String newName) throws Exception {
        try {
            if (getInfo(id).equals("character not exist")) {
                return getInfo(id);
            }
            Connection conn = DatabaseConnectionProvider.createConnection(configuration);
            Statement statement = conn.createStatement();

            String updateNameQuery = "UPDATE `character` SET name = \'" + newName + "\' WHERE id = " + id;
            statement.executeUpdate(updateNameQuery);
            conn.close();
            return "Updating info completed.";
        } catch (Exception error) {
            return String.format("Bad command: rename-character <character id> <new name>");
        }
    }
}
