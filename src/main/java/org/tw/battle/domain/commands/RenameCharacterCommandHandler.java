package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

import static java.lang.Integer.parseInt;


public class RenameCharacterCommandHandler implements CommandHandler {

    private static final String CMD_NAME = "rename-character";
    private final CharacterRepository characterRepository;

    public RenameCharacterCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if (commandArgs.length != 2) {
            return CommandResponse.fail(String.format("Bad command: %s <character id> <new name>", CMD_NAME));
        }

        // check if the id provided is valid integer
        try {
            parseInt(commandArgs[0]);
        } catch (Exception error) {
            return CommandResponse.fail(String.format("Bad command: %s <character id> <new name>", CMD_NAME));
        }

        // first check name length and if it contains illegal characters
        String newName = commandArgs[1];
        if (newName.length() < 3 || newName.length() > 64 || !newName.matches("^[a-zA-Z-]*$")){
            return CommandResponse.fail(String.format("Bad command: %s <character id> <new name>", CMD_NAME));
        } else {
            String updateResult = characterRepository.updateName(parseInt(commandArgs[0]), newName);
            if (updateResult.equals("Updating info completed.")) {
                return CommandResponse.success("Bad command: " + updateResult);
            } else {
                return CommandResponse.fail("Bad command: " + updateResult);
            }
        }
    }
}
