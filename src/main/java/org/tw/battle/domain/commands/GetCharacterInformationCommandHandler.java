package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;

import static java.lang.Integer.parseInt;

/**
 * @author Liu Xia
 */
public class GetCharacterInformationCommandHandler implements CommandHandler {
    private static final String CMD_NAME = "character-info";
    private final CharacterRepository characterRepository;

    public GetCharacterInformationCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return this.CMD_NAME == commandName;
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if (commandArgs.length != 1) {
            return CommandResponse.fail(String.format("Bad command: %s <character id>", CMD_NAME));
        }

        int id = parseInt(commandArgs[0]);
        final String characterInfo = characterRepository.getInfo(id);
        return characterInfo.equals("character not exist") ? CommandResponse.fail("Bad command: " + characterInfo) :
                CommandResponse.success(characterInfo);
    }
}
