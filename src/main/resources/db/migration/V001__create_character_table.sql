CREATE TABLE `character` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(64) DEFAULT 'unnamed',
	`hp` INT DEFAULT 100,
    `x` INT DEFAULT 0,
    `y` INT DEFAULT 0,
    PRIMARY KEY (id)
);